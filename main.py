# Snake Tutorial Python

import math
import random
import pygame
import time
import sys



class cube(object):
    rows = 20
    w = 500

    def __init__(self, start, dirnx=1, dirny=0, color=(255, 0, 0)):
        self.pos = start
        self.dirnx = 1
        self.dirny = 0
        self.color = color

    def move(self, dirnx, dirny):
        self.dirnx = dirnx
        self.dirny = dirny
        self.pos = (self.pos[0] + dirnx, self.pos[1] + dirny)

    def draw(self, surface, eyes=False):
        dis = self.w // self.rows
        i = self.pos[0]
        j = self.pos[1]
        pygame.draw.rect(surface, self.color, (dis * i + 1, dis * j + 1, dis - 2, dis - 2))
        if eyes:  # Draws the eyes
            centre = dis // 2
            radius = 3
            circleMiddle = (i * dis + centre - radius, j * dis + 8)
            circleMiddle2 = (i * dis + dis - radius * 2, j * dis + 8)
            pygame.draw.circle(surface, (0, 0, 0), circleMiddle, radius)
            pygame.draw.circle(surface, (0, 0, 0), circleMiddle2, radius)


class snake(object):
    body = []  # a list of cube objects
    turns = {}  # a dict of turns

    def __init__(self, color, pos):
        self.color = color
        self.head = cube(pos)  # defines the head cube with position
        self.body.append(self.head)  # appends the body to the head cube
        self.dirnx = 0  # define the direction of movement
        self.dirny = 1

    def move(self):
        global flag
        for event in pygame.event.get():  # exit game if red button pressed
            if event.type == pygame.QUIT:
                flag = False

            keys = pygame.key.get_pressed()  # get dict of pressed keys. not that important here but for other games

            for key in keys:  # iterate through all pressed keys
                if keys[pygame.K_LEFT]:
                    self.dirnx = -1  # update position
                    self.dirny = 0
                    self.turns[self.head.pos[:]] = [self.dirnx,
                                                    self.dirny]  # at current position at entry with new direction
                    # this list keeps growing to tell snake segments when to turn
                elif keys[pygame.K_RIGHT]:
                    self.dirnx = 1
                    self.dirny = 0
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]

                elif keys[pygame.K_UP]:
                    self.dirnx = 0
                    self.dirny = -1
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]

                elif keys[pygame.K_DOWN]:
                    self.dirnx = 0
                    self.dirny = 1
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]

        for i, c in enumerate(self.body):  # iterates through all cubes in body, returning cube and index
            p = c.pos[:]  # copy position for each cube
            if p in self.turns:  # cube is at a turn position
                turn = self.turns[p]  # get turn data for that cube
                c.move(turn[0], turn[1])  # pass turn data to cube move function
                if i == len(self.body) - 1:  # if at last cube
                    self.turns.pop(p)  # remove turn data for that position
            else:
                if c.dirnx == -1 and c.pos[0] <= 0:  # if block has reached edge, move to opposite side of screen
                    c.pos = (c.rows - 1, c.pos[1])
                elif c.dirnx == 1 and c.pos[0] >= c.rows - 1:
                    c.pos = (0, c.pos[1])
                elif c.dirny == -1 and c.pos[1] <= 0:
                    c.pos = (c.pos[0], c.rows - 1)
                elif c.dirny == 1 and c.pos[1] >= c.rows - 1:
                    c.pos = (c.pos[0], 0)
                else:  # if not at edge and no button pressed, move on
                    c.move(c.dirnx, c.dirny)

    def reset(self, pos):
        # reset all variables
        self.head = cube(pos)
        self.body = []
        self.body.append(self.head)
        self.turns = {}
        self.dirnx = 0
        self.dirny = 1

    def addCube(self):
        tail = self.body[-1]
        dx, dy = tail.dirnx, tail.dirny
        if dx == 1 and dy == 0:
            self.body.append(cube((tail.pos[0] - 1, tail.pos[1])))
        elif dx == -1 and dy == 0:
            self.body.append(cube((tail.pos[0] + 1, tail.pos[1])))
        elif dx == 0 and dy == 1:
            self.body.append(cube((tail.pos[0], tail.pos[1] - 1)))
        elif dx == 0 and dy == -1:
            self.body.append(cube((tail.pos[0], tail.pos[1] + 1)))

        self.body[-1].dirnx = dx
        self.body[-1].dirny = dy

    def draw(self, surface):
        for i, c in enumerate(self.body):
            if i == 0:
                c.draw(surface, True)
            else:
                c.draw(surface)


def redrawWindow(surface):
    global width, rows, s, snack
    surface.fill((0, 0, 0))
    s.draw(surface)
    snack.draw(surface)
    pygame.display.update()


def randomSnack(rows, item):
    positions = item.body  # snake item

    while True:
        x = random.randrange(rows)
        y = random.randrange(rows)
        if len(list(filter(lambda z: z.pos == (x, y),
                           positions))):  # filter iterates through positions, passing objects to lambda function which returns true or false.
            # filter function returns indexes of true elements
            # get new random numbers if position is occupied by snake
            continue
        else:
            break
    return (x, y)


def text_objects(text, font):
    textSurface = font.render(text, True, (0, 128, 0))
    return textSurface, textSurface.get_rect()


def message_display(text, surface):
    global width, clock
    largeText = pygame.font.SysFont("comicsansms", 72)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((width // 2), (width // 2))
    surface.fill((255, 0, 0))
    surface.blit(TextSurf, (250 - TextSurf.get_width() // 2, 250 - TextSurf.get_height() // 2))
    pygame.display.flip()
    time.sleep(10)

def main():
    global width, rows, s, flag, snack, clock
    width = 500
    rows = 20
    pygame.init()
    win = pygame.display.set_mode((width, width))  # screen object
    s = snake((255, 0, 0), (10, 10))  # snake object
    snack = cube(randomSnack(rows, s), color=(0, 255, 0))

    clock = pygame.time.Clock()  # clock object

    flag = True
    # start main game loop
    while flag:
        for event in pygame.event.get():  # check for end of game
            if event.type == pygame.QUIT: flag = False
        pygame.time.delay(20)  # delay to ensure game is not too fast
        clock.tick(7)  # run game at 10 fps
        s.move()
        if s.body[0].pos == snack.pos:  # if head is on a snack, eat and generate new snack
            s.addCube()
            snack = cube(randomSnack(rows, s), color=(0, 255, 0))

        for x in range(len(s.body)):  # iterates through all elements in the body
            if s.body[x].pos in list(map(lambda z: z.pos, s.body[
                                                          x + 1:])):  # checks if body at x has same position as any parts after it
                # map(fun, iter) applies fun to all items in iter
                # list converts tuple to list. Is it necessary here?
                message_display("You lose!", win)
                print('Score: ', len(s.body))
                s.reset((10, 10))
                break
        redrawWindow(win)

    pygame.quit()


main()
